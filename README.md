# code-challenge-digital-republic

Repositório referente ao desafio proposto para o processo seletivo da @digitalrepublic.
 @thiagomontini @nicoleteisen

## Objetivo
Uma aplicação web ou mobile que ajude o usuário a calcular a quantidade de tinta necessária para pintar uma sala.
Essa aplicação deve considerar que a sala é composta de 4 paredes e deve permitir que o usuário escolha qual a medida de cada parede e quantas janelas e portas possuem cada parede.
Com base na quantidade necessária o sistema deve apontar tamanhos de lata de tinta que o usuário deve comprar, sempre priorizando as latas maiores.

---

## Stack

Node.js

## Instalação

1. Clone o repositório
# Via HTTPS:
  * `https://gitlab.com/anacarol-r-santos/code-challenge-digital-republic.git`.

# Via SSH:  
  * `git clone git@gitlab.com:anacarol-r-santos/code-challenge-digital-republic.git`.

2. Entre na pasta do repositório que você acabou de clonar:
    * `code-challenge-digital-republic`

3. Instale as dependências
  * `npm install`

4. Inicie a aplicação
  * `npm start`

---

## Endpoint

- .POST/
- O corpo da requisição deverá ter o seguinte formato:

  ```json
  {    
    "wall1": {
        "heigth": "float",
        "base": "float",
        "qtdWindow": "integer",
        "qtdDoor": "integer"
    },
    "wall2": {
        "heigth": "float",
        "base": "float",
        "qtdWindow": "integer",
        "qtdDoor": "integer"
    },
    "wall3": {
        "heigth": "float",
        "base": "float",
        "qtdWindow": "integer",
        "qtdDoor": "integer"
    },
    "wall4": {
        "heigth": "float",
        "base": "float",
        "qtdWindow": "integer",
        "qtdDoor": "integer"
    }
  }
  ```

  - O retorno da requisição deverá ser assim:

  ```json
  [
    {
      "quantity": "integer",
      "size": 18
    },
    {
      "quantity": "integer",
      "size": 3.6
    },
    {
      "quantity": "integer",
      "size": 2.5
    },
    {
      "quantity": "integer",
      "size": 0.5
    }
  ]


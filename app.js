const express = require('express');
const { validationInputs } = require('./src/services/validations');
const { calculateCans } = require('./src/controller/calculateCans');
const { err } = require('./src/middlewares/err');

const app = express();
const port = 3000;

app.use(express.json());

app.post('/', validationInputs, calculateCans);

app.use(err);

app.listen(port, () => {
  console.log((`Listen on port ${port}`));
});

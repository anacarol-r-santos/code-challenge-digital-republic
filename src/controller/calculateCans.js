const { calculateCansForArea } = require('../services/calculateCansForArea');

const calculateCans = (req, res, next) => {
  try {
    const { body } = req;
    const cans = calculateCansForArea(body);
    res.status(200).json(cans);
  } catch (error) {
    next(error);
  }
};

module.exports = { calculateCans };

const err = (error, _req, res, next) => {
  if (process.env.NODE_ENV === 'development') {
    return next(err);
  }
  if (!error.status) {
    return res.status(500).json({ message: 'Internal server error' });
  }
  return res.status(error.status).json(error);
};

module.exports = { err };

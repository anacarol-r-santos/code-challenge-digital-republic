const { calculateQuantityCans } = require('./calculateQuantityCans');

const windowArea = 2.4;
const doorArea = 1.52;

const calculateCansForArea = (body) => {
  let area = 0;
  Object.keys(body).forEach((key) => {
    area += body[key].base * body[key].heigth;
    if (body[key].qtdWindow > 0 || body[key].qtdDoor > 0) {
      const element = body[key];
      area -= (element.qtdWindow * windowArea) + (element.qtdDoor * doorArea);
    }
  });
  const qtdCans = calculateQuantityCans(area);
  return qtdCans;
};

module.exports = { calculateCansForArea };

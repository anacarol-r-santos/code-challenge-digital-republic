const cansSizes = {
  L1: 18,
  L2: 3.6,
  L3: 2.5,
  L4: 0.5,
};
const literCapacity = 5;

// Calcula quantidade de latas de 18L
const calculateCan1 = (area) => {
  const literPerArea = area / literCapacity;
  return Math.floor(literPerArea / cansSizes.L1);
};

// Calcula quantidade de latas de 3,6L
const calculateCan2 = (area) => {
  const literPerArea = area / literCapacity;
  const can1 = calculateCan1(area);
  return Math.floor((literPerArea - (cansSizes.L1 * can1)) / cansSizes.L2);
};

// Calcula quantidade de latas de 2,5L
const calculateCan3 = (area) => {
  const literPerArea = area / literCapacity;
  const can1 = calculateCan1(area);
  const can2 = calculateCan2(area);
  const op = literPerArea - (cansSizes.L1 * can1) - (cansSizes.L2 * can2);
  if (op > 2 && op < 2.5) {
    return Math.ceil(op / cansSizes.L3);
  }
  return Math.floor(op / cansSizes.L3);
};

// Calcula quantidade de latas de 0,5L
const calculateCan4 = (area) => {
  const literPerArea = area / literCapacity;
  const can1 = calculateCan1(area);
  const can2 = calculateCan2(area);
  const can3 = calculateCan3(area);

  return Math.ceil((
    literPerArea - (cansSizes.L1 * can1) - (cansSizes.L2 * can2)
        - (cansSizes.L3 * can3)
  ) / cansSizes.L4);
};

const calculateQuantityCans = (area) => {
  const can1 = calculateCan1(area);
  const can2 = calculateCan2(area);
  const can3 = calculateCan3(area);
  const can4 = calculateCan4(area);

  const possibilities = [can1, can2, can3, can4];
  const cans = [cansSizes.L1, cansSizes.L2, cansSizes.L3, cansSizes.L4];

  const result = [{ quantity: 0, size: 0 }];
  for (let i = 0; i < possibilities.length; i += 1) {
    for (let j = 0; j < cans.length; j += 1) {
      if (i === j) {
        result.push({ quantity: possibilities[i], size: cans[j] });
      }
    }
  }
  return result.slice(1);
};

module.exports = { calculateQuantityCans };

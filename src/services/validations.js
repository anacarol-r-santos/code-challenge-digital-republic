const validationInputs = (req, _res, next) => {
  const { body } = req;

  Object.keys(body).forEach((key) => {
    const wallArea = body[key].base * body[key].heigth;
    const windowArea = body[key].qtdWindow * 2.4;
    const doorArea = body[key].qtdDoor * 1.52;
    const heigthDoor = 1.9;

    if (!body[key].base || !body[key].heigth) {
      return next({ status: 400, message: 'É preciso informar a base e altura' });
    }
    if (wallArea < 1 || wallArea > 50) {
      return next({ status: 400, message: 'A área total deve estar entre 1m² e 50m²' });
    }
    if (
      (windowArea * body[key].qtdWindow) + (doorArea * body[key].qtdDoor)
      > wallArea / 2
    ) {
      return next({
        status: 400,
        message:
          'O total de área das portas e janelas deve ser no máximo 50% da área de parede',
      });
    }
    if (body[key].qtdDoor > 0 && body[key].heigth < heigthDoor + 0.3) {
      return next({
        status: 400,
        message:
          'A altura de paredes com porta deve ser, no mínimo, 30 centímetros maior que a altura da porta',
      });
    }
    return true;
  });
  next();
};

module.exports = { validationInputs };
